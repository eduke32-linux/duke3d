﻿
![picture](https://cdn2.steamgriddb.com/logo_thumb/5f5c19fa671886b5f7f205d541157c1f.png) 

Duke Nukem 3D setup/launch script powered by EDuke32, this is also a set of AUR packages.

The binaries were compiled from the official Eduke32 repo
https://voidpoint.io/terminx/eduke32

This comes with Duke Nukem 3D: Atomic

Packages: 
[duke3d](https://aur.archlinux.org/packages/duke3d))
[eduke32-bin](https://aur.archlinux.org/packages/eduke32-bin))

 ### Author
  * Corey Bruce
